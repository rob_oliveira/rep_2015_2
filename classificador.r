source("classificadores/prepare.r")
source("classificadores/dmc.r")
source("classificadores/naive.r")
source("classificadores/bayes.r")
source("classificadores/knn.r")
source("classificadores/em.r")





cat("classificadores para a Base de dados Iris\n")

 cat("\n------------------------------------------\n")
 cat("Matriz de Confusao para o classificador DMC.\n")
 trainingDmc(trainingFold, testFold)

# cat("\n------------------------------------------\n")


 cat("Matriz de Confusao para o classificador Naive Bayes.\n")
 trainingNaive(trainingFold, testFold)
 cat("\n------------------------------------------\n")

 cat("Matriz de Confusao para o classificador Bayes Gaussiano.\n")
 trainingBayes(trainingFold, testFold)
 cat("\n------------------------------------------\n")
# #print("Matriz de Confusao para o estimador Nadaraya Watson")
# #trainingNadaraya(trainingFold, testFold)

 cat("Matriz de Confusao para o classificador 5NN.\n")
 knn(5, trainingFold, testFold)
 cat("\n------------------------------------------\n")

#cat("Matriz de Confusao para o EM.\n")
#expectationMaximization(trainingFold, testFold)
#cat("\n------------------------------------------\n")
