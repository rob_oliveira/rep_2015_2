# REP 2015.2 #

Este repositório foi criado para armazenar os códigos dos trabalhos de programação da disciplina de Reconhecimento Estatístico de Padrões, Ministrada no Mestrada em Ciência da Computação na Universidade Estadual do Ceara.

Os classificadores foram feitos utilizando a base de dados Iris disponivel neste [endereço](https://archive.ics.uci.edu/ml/datasets/Iris)

### Modo de usar ###

Após a instalação do R, basta dar o seguinte comando pelo terminal:

* Rscript classificador.r
