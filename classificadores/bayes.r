

extractProbability <- function(x, class, mean){

    x$V5 <- NULL
    x = matrix(unlist(x), ncol=4, nrow=1)
    mean = matrix(unlist(mean), ncol=4, nrow=1)
    cov_matrix = cov(class)

    aux = (x - mean)%*%solve(cov_matrix)%*%t(x-mean)
    px1 = 1/(2*pi)^2 * sqrt(norm(cov_matrix)) * exp(-0.5 * aux)
    return(px1[1,1])
}

trainingBayes <- function(trData, testData){
    dados_virginica = trData[trData$V5 == virginica, ]
    dados_setosa = trData[trData$V5 == setosa, ]
    dados_versicolor = trData[trData$V5 == versicolor, ]

    dados_virginica$V5 <- NULL
    dados_setosa$V5 <- NULL
    dados_versicolor$V5 <- NULL

    centro_virginica = colMeans(dados_virginica)
    centro_setosa = colMeans(dados_setosa)
    centro_versicolor = colMeans(dados_versicolor)

    testConfusion = confusao
    for (i in 1:nrow(testData)) {
        pvirginica = extractProbability(testData[i,], dados_virginica, centro_virginica)
        psetosa = extractProbability(testData[i,], dados_setosa, centro_setosa)
        pversicolor = extractProbability(testData[i,], dados_versicolor, centro_versicolor)

        classe = ""
        if(pvirginica > psetosa && pvirginica > pversicolor){
          classe = virginica
        }
        else if(psetosa > pvirginica && psetosa > pversicolor){
          classe =  setosa
        }
        else if(pversicolor > pvirginica && pversicolor > psetosa){
          classe =  versicolor
        }
        testConfusion = testConfusion + contruirConfusao(classe, testData[i,])
    }
    print(testConfusion)
    cat("Acuracia do Classificador de Bayes:\t")
    print(sum(diag(testConfusion))/sum(testConfusion))

    cat("centro da classe virginica:\n")
    print(centro_virginica)
    cat("centro da classe setosa:\n")
    print(centro_setosa)
    cat("centro da classe versicolor:\n")
    print(centro_versicolor)

    cat("matriz de covariancia da classe virginica:\n")
    print(cov(dados_virginica))
    cat("matriz de covariancia da classe setosa:\n")
    print(cov(dados_setosa))
    cat("matriz de covariancia da classe versicolor:\n")
    print(cov(dados_versicolor))

}