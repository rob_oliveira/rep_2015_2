
kdistance <- function(k, teste, trainingData){

    k_vectors = matrix(ncol=ncol(trainingData), nrow=k)
    test_point = teste[, -5]

    for (i in 1:k) {
        distance = 1
        min_distance = dist(rbind(trainingData[1, -5], test_point))
        k_vectors[i,] = unlist(trainingData[1, ])
        min_pos = 1
        for (j in 2:nrow(trainingData)) {
            vet = trainingData[j, -5]
            new_dist = dist(rbind(vet, test_point))
            if(min_distance > new_dist){
                min_pos = j
                min_distance = new_dist
                k_vectors[i,] <- as.vector(unlist(trainingData[j, ]))
            }
        }
        trainingData = trainingData[-min_pos, ]
    }

    class = table(k_vectors[, 5])   #essas duas linhas server pra extrair o valor da classe mais frequente
    class = names(class)[which.max(class)]
    if(class == "1"){return = virginica}
    else if(class == "2"){return = setosa}
    else if(class == "3"){return = versicolor}
    
}

knn <- function(k, trData, testData){
    dados_virginica = trData[trData$V5 == virginica, ]
    dados_virginica$V5 = 1
    dados_setosa = trData[trData$V5 == setosa, ]
    dados_setosa$V5 = 2
    dados_versicolor = trData[trData$V5 == versicolor, ]
    dados_versicolor$V5 = 3

    aux_trData = rbind(dados_virginica, dados_setosa)
    aux_trData = rbind(aux_trData, dados_versicolor)

    testConfusion = confusao
    for (i in 1:nrow(testData)) {
        classe = kdistance(k, testData[i, ], aux_trData)
        testConfusion = testConfusion + contruirConfusao(classe, testData[i,])
    }
    print(testConfusion)
    cat("Acuracia do Classificador de 5NN:\t")
    print(sum(diag(testConfusion))/sum(testConfusion))
}
