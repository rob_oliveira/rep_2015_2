

testDistance <- function(cvirginica, csetosa, cversicolor, testData){

    testConfusion = confusao
    for (i in 1:nrow(testData)) {
        teste = testData[i , -5]
        d1 = dist(rbind(cvirginica, teste))
        d2 = dist(rbind(csetosa, teste))
        d3 = dist(rbind(cversicolor, teste))

        classe = ""
        if(d1 < d2 && d1 < d3){
          classe = virginica
        }
        else if(d2 < d1 && d2 < d3){
          classe =  setosa
        }
        else if(d3 < d1 && d3 < d2){
          classe =  versicolor
        }
        testConfusion = testConfusion + contruirConfusao(classe, testData[i,])
    }
    print(testConfusion)
    cat("Acuracia do Classificador DMC:\t")
    print(sum(diag(testConfusion))/sum(testConfusion))
    cat("centro da classe virginica:\n")
    print(cvirginica)
    cat("centro da classe setosa:\n")
    print(csetosa)
    cat("centro da classe versicolor:\n")
    print(cversicolor)

    
}
trainingDmc <- function(trData, testData){

    dados_virginica = trData[trData$V5 == virginica, ]
    dados_setosa = trData[trData$V5 == setosa, ]
    dados_versicolor = trData[trData$V5 == versicolor, ]

    dados_virginica$V5 <- NULL
    dados_setosa$V5 <- NULL
    dados_versicolor$V5 <- NULL

    centro_virginica = colMeans(dados_virginica)
    centro_setosa = colMeans(dados_setosa)
    centro_versicolor = colMeans(dados_versicolor)

 
    testDistance(centro_virginica, centro_setosa, centro_versicolor, testData)
}
