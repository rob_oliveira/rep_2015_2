
library(gdata)
data = read.csv("Iris/iris.data",  header=FALSE)
shufledData <- data[sample(nrow(data)),]

confusao = matrix( c(0,0,0,0,0,0,0,0,0), nrow=3, ncol=3, byrow=TRUE)
rownames(confusao) <- c("virginica", "setosa", "versicolor")
colnames(confusao) <- c("virginica", "setosa", "versicolor")

testFold = shufledData
trainingFold = shufledData

virginica = "Iris-virginica"
setosa = "Iris-setosa"
versicolor = "Iris-versicolor"

#print(nrow(testFold))
#print(nrow(trainingFold))
contruirConfusao <- function(classe, valorTeste){

    if(valorTeste[, 5] ==  virginica){
    
        if(classe == valorTeste[, 5]){ confusao[1,1] = confusao[1,1] + 1}
        else if(classe ==  setosa){confusao[1,2] = confusao[1,2] + 1}
        else if(classe ==  versicolor){confusao[1,3] = confusao[1,3] + 1}
    
    }
    else if(valorTeste[, 5] ==  setosa){
    
        if(classe == valorTeste[, 5]){ confusao[2,2] = confusao[2,2] + 1}
        else if(classe ==  virginica){confusao[2,1] = confusao[2,1] + 1}
        else if(classe ==  versicolor){confusao[2,3] = confusao[2,3] + 1}
    
    }
    else if(valorTeste[, 5] ==  versicolor){
    
        if(classe == valorTeste[, 5]){ confusao[3,3] = confusao[3,3] + 1}
        else if(classe ==  virginica){confusao[3,1] = confusao[3,1] + 1}
        else if(classe ==  setosa){confusao[3,2] = confusao[3,2] + 1}
    }

    return(confusao)
}
