

norm_vector <- function(v){
    v = matrix(unlist(v))
    sqrt((t(v)%*%v)[1,1])
}


k_means <- function(k, valores_treinamento){
    valores_treinamento = valores_treinamento[, -5]
    valores_treinamento = data.matrix(valores_treinamento)
    centers = matrix(ncol=ncol(valores_treinamento), nrow=k)


    for(i in 1:k){
        centers[i, ] = valores_treinamento[i, ]
    }
 

    centers_old = centers
    cluster_count = rep(0,k)
    variancia = rep(0, k)
    diff = 1
    while(diff > 1e-4){ #2nn 
        centers_old = centers
        clusters = matrix(0, ncol=ncol(valores_treinamento), nrow=k)
        cluster_count = rep(1,k)
        variancia = rep(0, k)
        for (i in 1:nrow(valores_treinamento)) {
            min = 1
            distance_old = norm_vector(valores_treinamento[i, ] - centers[1,])
            
            for (j in 1:k) {
                distance =  norm_vector(valores_treinamento[i, ] - centers[j,])
                if(distance < distance_old){
                    min = j
                }
                distance_old = distance
            }
            
            clusters[min,] = valores_treinamento[i, ] + clusters[min,]
            cluster_count[min] = cluster_count[min] +1
            
        }
        for (j in 1:k) {
            centers[j,] = clusters[j,]/cluster_count[j]
            diff = diff + norm_vector(centers[j, ] - centers_old[j,])
        }
        
        diff = diff/nrow(centers)
    }
    return(centers)
}

gaussian <- function(x, covariance){
    x = matrix(x, ncol=ncol(covariance))
    aux = (x)%*%solve(covariance)%*%t(x)
    px = 1/(2*pi)^2 * sqrt(norm(covariance)) * exp(-0.5 * aux)
    return(px[1,1])
}

expectationMaximization <- function(testFold, trainingFold){

    dados_virginica = trainingFold[trainingFold$V5 == virginica, ]
    dados_setosa = trainingFold[trainingFold$V5 == setosa, ]
    dados_versicolor = trainingFold[trainingFold$V5 == versicolor, ]

    dados_virginica$V5 <- NULL
    dados_setosa$V5 <- NULL
    dados_versicolor$V5 <- NULL

    centro_virginica = colMeans(dados_virginica)
    centro_setosa = colMeans(dados_setosa)
    centro_versicolor = colMeans(dados_versicolor)

    k = 3
    centers = k_means(k, trainingFold)
    trainingMatrix = data.matrix(trainingFold[, -5])
    auxCov = sweep(trainingMatrix, 2, centers[1, ])
    covList = list(cov(auxCov))
    for (i in 2:k) {
        auxCov = sweep(trainingMatrix, 2, centers[i, ])
        covList[[i]] <- cov(auxCov)
    }

    #print(covList)
    probWeigth = matrix(ncol = k, nrow = nrow(trainingMatrix))
    sumProb = matrix(rep(1/3, k), ncol = k, nrow = 1)

    for(l in 1:20){
        
        for (i in 1:nrow(trainingMatrix)) { #E
            for (j in 1:ncol(probWeigth)) {
                covMat =  matrix(unlist(covList[[j]]), ncol=ncol(trainingMatrix))
                probWeigth[i,j] = 1/sumProb[1, k] * gaussian(trainingMatrix[i, ] - centers[j, ], 1/sumProb[1,k]*covMat)
            }
            probWeigth[i,] = probWeigth[i,]/sum(probWeigth[i,])

        }
        
        for (i in 1:k) { #M
            sumProb[1,i] = sum(probWeigth[,i])/nrow(probWeigth)

            centers[i,] = trainingMatrix[1,] * 1/sumProb[1,i]
            for (j in 2:nrow(trainingMatrix)) {
                centers[i,] = centers[i,] + trainingMatrix[j,]/sumProb[1,i] 
            }
            centers[i, ] = centers[i, ]/nrow(trainingMatrix)
        }
        print(centers)

    }

}
